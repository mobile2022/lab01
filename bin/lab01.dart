import 'dart:io';

import 'package:lab01/lab01.dart' as lab01;

void main(List<String> arguments) {
  // calculator();
  countWord();
}

void countWord() {
  print("Please input your Message");
  String input = stdin.readLineSync()!;
  input.toLowerCase();

  List word = input.split(' ');
  // print(word);

  int count = 1;

  for (int i = 0; i < word.length; i++) {
    for (int j = i; j < word.length; j++) {
      if (word[j] == word[i] && j != i) {
        word.removeAt(j);
        count++;
        j--;
      } else if ((i == word.length) && (j == word.length)) {
        word.removeAt(j);
        count++;
        j--;
      }
    }
    // print(word[i]+" ");
    stdout.write(" " + word[i] + ":");
    stdout.write(count);
    // print(count);
    count = 1;
  }
}

int calculator() {
  print("MENU\n");
  print("Select the choice you want to perform : \n");
  print("1 ADD\n");
  print("2 SUBTRACT\n");
  print("3 MULTIPLY\n");
  print("4 DIVIDE\n");
  print("5 EXIT\n");
  int a = int.parse(stdin.readLineSync()!);
  switch (a) {
    case 1:
      {
        print("Please input your number x :");
        double a = double.parse(stdin.readLineSync()!);
        print("Please input your number y :");
        double b = double.parse(stdin.readLineSync()!);
        print(Add(a, b));
      }
      break;
    case 2:
      {
        print("Please input your number x :");
        double a = double.parse(stdin.readLineSync()!);
        print("Please input your number y :");
        double b = double.parse(stdin.readLineSync()!);
        print(Subtract(a, b));
      }
      break;
    case 3:
      {
        print("Please input your number x :");
        double a = double.parse(stdin.readLineSync()!);
        print("Please input your number y :");
        double b = double.parse(stdin.readLineSync()!);
        print(Multiply(a, b));
      }
      break;
    case 4:
      {
        print("Please input your number x :");
        double a = double.parse(stdin.readLineSync()!);
        print("Please input your number y :");
        double b = double.parse(stdin.readLineSync()!);
        print(Divide(a, b));
      }
      break;
    case 5:
      {
        break;
      }
  }
  return 1;
}

double Divide(double a, double b) {
  return a / b;
}

double Multiply(double a, double b) {
  return a * b;
}

double Subtract(double a, double b) {
  return a - b;
}

double Add(double a, double b) {
  return a + b;
}
